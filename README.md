# Example of Motor control with PMW on a ESP32

This code is structured to be used on VSCode using PlatformIO, it assumes you connected 2 drivers to the ESP32, and make them turn, one full speed, the other at half speed.