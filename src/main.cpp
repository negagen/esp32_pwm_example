#include <Arduino.h>
#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"

// the number of the pin
const int pinPwm0_a = 25; //
const int pinPwm0_b = 26; //
const int pinPwm1_a = 27; //
const int pinPwm1_b = 14; //

const mcpwm_config_t config_0 = {
  5000, // freq
  100.0, // cycle  A
  0.0, // cycle B
  MCPWM_DUTY_MODE_0,
  MCPWM_UP_COUNTER
};

const mcpwm_config_t config_1 = {
  5000, // freq
  60.0, // cycle  A
  0.0, // cycle B
  MCPWM_DUTY_MODE_0,
  MCPWM_UP_COUNTER
};

int initPwm(){
  return
  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, pinPwm0_a)  ||
  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0B, pinPwm0_b)  ||
  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, pinPwm1_a)  ||
  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0B, pinPwm1_b)  ||
  mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &config_0) ||
  mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_1, &config_1) ? 
  ESP_ERR_INVALID_ARG                                :
  ESP_OK                                             ;
}

void setup() {
  Serial.begin(9600);
  delay(1000); // Wait a second
  if(initPwm()){
    Serial.print(F("Error el control de PWM"));
    while(true){};
  }
  Serial.println("All green!");
  delay(3000);
}
 
void loop(){
  for(float dutyCycle = 50.0; dutyCycle <= 100; dutyCycle=dutyCycle+0.1){   
    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, dutyCycle);
    mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B);
    delay(15);
  }

  delay(3000);

  for(float dutyCycle = 100.0; dutyCycle >= 50; dutyCycle=dutyCycle-0.1){
    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, dutyCycle);
    mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B);
    delay(15);
  }
}